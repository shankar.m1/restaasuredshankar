package apiTesting;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;


import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class putOperation {
  @Test
  public void updatingDatausingPut() {
	  JSONObject request= new JSONObject();
	  request.put("name", "shnakar");
	  request.put("job", "trainee");
	  
	 System.out.println(request);
	 baseURI="https://reqres.in/api";
	 given().body(request.toJSONString()).when().post("/users").then().statusCode(200).body("updateAt", greaterThan("2023-01-24T09:50:12:293Z"));
	 
  }
}
