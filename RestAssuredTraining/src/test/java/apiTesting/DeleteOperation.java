package apiTesting;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.testng.annotations.Test;
import static org.hamcrest.Matchers.*;

public class DeleteOperation {
	@Test
	public void f() {
		baseURI = "https://reqres.in";
		given().when().delete("api/users/2").then().statusCode(204);

	}
} 
