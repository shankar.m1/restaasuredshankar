package apiTesting;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import org.json.simple.JSONObject;
import static org.hamcrest.Matchers.*;

import org.testng.annotations.Test;

public class PostOperation {
  @Test
  public void f() {
	  JSONObject request= new JSONObject();
	  request.put("name", "shankar");
	  request.put("job", "trainee");
	  
	 System.out.println(request);
	 baseURI="https://reqres.in/api";
	 given().body(request.toJSONString()).when().post("/users").then().statusCode(201);
	 
  }
}
