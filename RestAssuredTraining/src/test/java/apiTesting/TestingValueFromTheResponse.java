package apiTesting;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.testng.annotations.Test;
import static org.hamcrest.Matchers.*;


public class TestingValueFromTheResponse {
  @Test
  public void testinValueFromTheResponse() {
	  baseURI="https://reqres.in";
	  given().get("/api/users?page=2").then().body("data.first_name",hasItem("Byron"));
  }
  @Test
  public void testingMultipleValues() {
	  baseURI="https://reqres.in";
	  given().get("/api/users?page=2").then().body("data.first_name",hasItems("Byron","Michael"));
	  

}
}
